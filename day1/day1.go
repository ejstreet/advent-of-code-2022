package day1

import (
	"fmt"
	"sort"
	"strconv"
	"strings"

	"gitlab.com/ejstreet/advent-of-code-2022/common"
)

// Find the Elf carrying the most Calories. How many total Calories is that Elf carrying?
func d1a(input string) int {
	arrayOfCalories := strings.Split(input, "\n")

	maxTotal := 0
	currentTotal := 0

	for i := range arrayOfCalories {
		if arrayOfCalories[i] != "" {
			cals, _ := strconv.Atoi(arrayOfCalories[i])
			currentTotal += cals
		} else {
			if currentTotal > maxTotal {
				maxTotal = currentTotal
			}
			currentTotal = 0
		}
	}

	return maxTotal
}

// Find the top three Elves carrying the most Calories. How many Calories are those Elves carrying in total?
func d1b(input string) int {
	arrayOfCalories := strings.Split(input, "\n")

	currentTotal := 0
	arrayOfTotals := make([]int, 0)

	for i := range arrayOfCalories {
		if arrayOfCalories[i] != "" {
			cals, _ := strconv.Atoi(arrayOfCalories[i])
			currentTotal += cals
		} else {
			arrayOfTotals = append(arrayOfTotals, currentTotal)
			currentTotal = 0
		}
	}

	totalsSlice := arrayOfTotals[:]
	sort.Sort(sort.Reverse(sort.IntSlice(totalsSlice)))

	return arrayOfTotals[0] + arrayOfTotals[1] + arrayOfTotals[2]
}

func RunD1a() {
	fmt.Println("Day 1A:", d1a(common.GetInput("day1/input.txt")))
}

func RunD1b() {
	fmt.Println("Day 1B:", d1b(common.GetInput("day1/input.txt")))
}
