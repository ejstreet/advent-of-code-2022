package day1

import "testing"

const test_input = `1000
2000
3000

4000

5000
6000

7000
8000
9000

10000
`

func TestDay1a(t *testing.T) {
	result := d1a(test_input)
	expected := 24000
	if result != expected {
		t.Log("Error: result should be ", expected, "- but got", result)
		t.Fail()
	}
}

func TestDay1b(t *testing.T) {
	result := d1b(test_input)
	expected := 45000
	if result != expected {
		t.Log("Error: result should be ", expected, "- but got", result)
		t.Fail()
	}
}
