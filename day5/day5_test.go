package day5

import "testing"

const test_input = `    [D]    
[N] [C]    
[Z] [M] [P]
 1   2   3 

move 1 from 2 to 1
move 3 from 1 to 3
move 2 from 2 to 1
move 1 from 1 to 2
`

func TestDay5a(t *testing.T) {
	result := d5a(test_input)
	expected := "CMZ"
	if result != expected {
		t.Log("Error: result should be ", expected, "- but got", result)
		t.Fail()
	}
}

func TestDay5b(t *testing.T) {
	result := d5b(test_input)
	expected := "MCD"
	if result != expected {
		t.Log("Error: result should be ", expected, "- but got", result)
		t.Fail()
	}
}
