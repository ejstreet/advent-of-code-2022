package day5

import (
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/ejstreet/advent-of-code-2022/common"
)

func parseCratesToArrays(rawCrates string) [][]string {
	rows := strings.Split(rawCrates, "\n")
	yMax := len(rows) - 2
	xMax := len(strings.Split(rows[len(rows)-1], "  "))

	transposedColumns := make([][]string, xMax)

	for x := 0; x < xMax; x++ {
		for y := yMax; y >= 0; y-- {
			val := string(rows[y][4*x+1])
			if val != " " {
				transposedColumns[x] = append(transposedColumns[x], val)
			}
		}
	}

	return transposedColumns
}

func atoi(s string) int {
	val, _ := strconv.Atoi(s)
	return val
}

func reverseArray(input []string) {
	for i, j := 0, len(input)-1; i < j; i, j = i+1, j-1 {
		input[i], input[j] = input[j], input[i]
	}
}

func runProcedures(crates [][]string, procedures []string, multipick bool) {
	for p := 0; p < len(procedures); p++ {
		if procedures[p] != "" {
			proc := strings.Split(procedures[p], " ")

			count := atoi(proc[1])
			orig := atoi(proc[3]) - 1
			dest := atoi(proc[5]) - 1

			move := crates[orig][len(crates[orig])-count:]
			if !multipick {
				reverseArray(move)
			}

			crates[orig] = crates[orig][:len(crates[orig])-count]
			crates[dest] = append(crates[dest], move...)
		}
	}
}

func moveCrates(input string, multipick bool) string {
	rawCrates := strings.Split(input, "\n\n")[0]
	rawProcedures := strings.Split(input, "\n\n")[1]

	crates := parseCratesToArrays(rawCrates)
	procedures := strings.Split(rawProcedures, "\n")

	runProcedures(crates, procedures, multipick)

	output := ""
	for x := 0; x < len(crates); x++ {
		output = output + crates[x][len(crates[x])-1]
	}

	return output
}

func d5a(input string) string {
	return moveCrates(input, false)
}

func d5b(input string) string {
	return moveCrates(input, true)
}

func RunD5a() {
	fmt.Println("Day 5A:", d5a(common.GetInput("day5/input.txt")))
}

func RunD5b() {
	fmt.Println("Day 5B:", d5b(common.GetInput("day5/input.txt")))
}
