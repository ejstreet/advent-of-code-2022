# Advent of Code 2022

I've chosen to use the opportunity of Advent of Code this year to learn Go!

This project also employs a continuous integration pipeline that runs tests, format checks, and then builds all the solutions into a single binary, and then finally a docker container complete with input files.

![Gopher wearing a Santa hat](gopher.png)

## Running this project locally
To build this project, first make sure you have installed [Go](https://go.dev/doc/install). Then clone the repo using
```bash
git clone https://gitlab.com/ejstreet/advent-of-code-2022.git
```
Then change into the root of the directory and run
```bash
go run main.go all
```
This will run all solutions. To run a specific solution e.g. *Day 1 Part A*, run:
```bash
go run main.go 1 a
```

### Tests
This project employs tests to confirm that solutions work for the provided examples. It does not perform exhaustive testing on each of the individual functions (sorry).

To run tests, simply run 
```bash
go tests ./...
```
**Happy Holidays!**