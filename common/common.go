package common

import (
	"fmt"
	"os"
)

func GetInput(filename string) string {
	content, err := os.ReadFile(filename)

	if err != nil {
		fmt.Println(err)
	}

	return string(content)
}
