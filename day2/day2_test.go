package day2

import "testing"

const test_input = `A Y
B X
C Z
`

func TestDay2a(t *testing.T) {
	result := d2a(test_input)
	expected := 15
	if result != expected {
		t.Log("Error: result should be ", expected, "- but got", result)
		t.Fail()
	}
}

func TestDay2b(t *testing.T) {
	result := d2b(test_input)
	expected := 12
	if result != expected {
		t.Log("Error: result should be ", expected, "- but got", result)
		t.Fail()
	}
}
