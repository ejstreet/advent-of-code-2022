package day2

import (
	"fmt"
	"strings"

	"gitlab.com/ejstreet/advent-of-code-2022/common"
)

func getScoreKnownPlay(play string) int {
	oPlay := strings.Split(play, " ")[0]
	pPlay := strings.Split(play, " ")[1]

	oScores := map[string]int{"A": 1, "B": 2, "C": 3}
	pScores := map[string]int{"X": 1, "Y": 2, "Z": 3}

	outcomeScore := 0

	if pScores[pPlay] == oScores[oPlay]%3+1 {
		outcomeScore = 6 //Win
	} else if pScores[pPlay] == oScores[oPlay] {
		outcomeScore = 3 //Draw
	}

	return pScores[pPlay] + outcomeScore
}

func getScoreKnownOutcome(play string) int {
	oPlay := strings.Split(play, " ")[0]
	outcome := strings.Split(play, " ")[1]

	Scores := map[string]int{"A": 1, "B": 2, "C": 3}
	outcomeScores := map[string]int{"X": 0, "Y": 3, "Z": 6}

	var pScore int

	switch outcome {
	case "X": // Lose
		pScore = 1 + (Scores[oPlay]+1)%3
	case "Y": // Draw
		pScore = Scores[oPlay]
	case "Z": // Win
		pScore = 1 + Scores[oPlay]%3
	}

	return pScore + outcomeScores[outcome]
}

// What would your total score be if everything goes exactly according to your strategy guide?
func d2a(input string) int {
	rounds := strings.Split(input, "\n")
	totalScore := 0

	for r := 0; r < len(rounds); r++ {
		if rounds[r] != "" {
			totalScore += getScoreKnownPlay(rounds[r])
		}
	}

	return totalScore
}

// Following the Elf's instructions for the second column, what would your total
// score be if everything goes exactly according to your strategy guide?
func d2b(input string) int {
	rounds := strings.Split(input, "\n")
	totalScore := 0

	for r := 0; r < len(rounds); r++ {
		if rounds[r] != "" {
			totalScore += getScoreKnownOutcome(rounds[r])
		}
	}

	return totalScore
}

func RunD2a() {
	fmt.Println("Day 2A:", d2a(common.GetInput("day2/input.txt")))
}

func RunD2b() {
	fmt.Println("Day 2B:", d2b(common.GetInput("day2/input.txt")))
}
