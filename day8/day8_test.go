package day8

import "testing"

const test_input = `30373
25512
65332
33549
35390
`

func TestDay8a(t *testing.T) {
	result := d8a(test_input)
	expected := 21
	if result != expected {
		t.Log("Error: result should be ", expected, "- but got", result)
		t.Fail()
	}
}

func TestDay8b(t *testing.T) {
	result := d8b(test_input)
	expected := 8
	if result != expected {
		t.Log("Error: result should be ", expected, "- but got", result)
		t.Fail()
	}
}
