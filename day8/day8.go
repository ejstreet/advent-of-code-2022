package day8

import (
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/ejstreet/advent-of-code-2022/common"
)

func byXY(x int, y int, mX int) int {
	return x + y*mX
}

func look(forest []int, visible []bool, mX int, mY int, direction string) {

	facing := map[string][]int{"N": {0, -1}, "S": {0, 1}, "E": {1, 0}, "W": {-1, 0}}
	sX, sY := 0, 0

	switch direction {
	case "N":
		sY = mY - 1
	case "W":
		sX = mX - 1
	}

	if direction == "E" || direction == "W" {
		i := facing[direction][0]

		for y := 0; y < mY; y++ {
			highest := -1
			for x := 0; x < mX; x++ {
				addr := byXY(sX+x*i, y, mX)
				if forest[addr] > highest {
					highest = forest[addr]
					visible[addr] = true
					if highest == 9 {
						break
					}
				}
			}
		}
	}

	if direction == "N" || direction == "S" {
		i := facing[direction][1]

		for x := 0; x < mX; x++ {
			highest := -1
			for y := 0; y < mY; y++ {
				addr := byXY(x, sY+y*i, mX)
				if forest[addr] > highest {
					highest = forest[addr]
					visible[addr] = true
					if highest == 9 {
						break
					}
				}
			}
		}
	}
}

func countTreesInFront(oX int, oY int, forest []int, axis string, d int, mX int) int {
	houseHeight := forest[byXY(oX, oY, mX)]
	count := 0

	switch axis {
	case "x":
		for i := d; oX+i < mX && oX+i >= 0; i += d {
			count++
			if forest[byXY(oX+i, oY, mX)] >= houseHeight {
				break
			}
		}
	case "y":
		mY := len(forest) / mX

		for i := d; oY+i < mY && oY+i >= 0; i += d {
			count++
			if forest[byXY(oX, oY+i, mX)] >= houseHeight {
				break
			}
		}
	}

	return count
}

func scorePosition(x int, y int, forest []int, mX int) int {
	totalScore := 1
	score := 0
	for _, a := range []string{"x", "y"} {
		for _, d := range []int{1, -1} {
			score = countTreesInFront(x, y, forest, a, d, mX)
			if score == 0 {
				return 0
			}
			totalScore = totalScore * score

		}
	}
	return totalScore
}

func d8a(input string) int {

	maxX := strings.Index(input, "\n")
	maxY := strings.Count(input, "\n")

	forest := make([]int, 0)

	for _, v := range strings.Replace(input, "\n", "", -1) {
		tree, _ := strconv.Atoi(string(v))
		forest = append(forest, tree)
	}

	visible := make([]bool, len(forest))

	for _, d := range []string{"N", "E", "S", "W"} {
		look(forest, visible, maxX, maxY, d)
	}

	totalVisible := 0
	for _, v := range visible {
		if v {
			totalVisible++
		}
	}

	return totalVisible
}

func d8b(input string) int {
	maxX := strings.Index(input, "\n")
	maxY := strings.Count(input, "\n")

	forest := make([]int, 0)

	for _, v := range strings.Replace(input, "\n", "", -1) {
		tree, _ := strconv.Atoi(string(v))
		forest = append(forest, tree)
	}

	topScore := 0

	for x := 0; x < maxX; x++ {
		for y := 0; y < maxY; y++ {

			score := scorePosition(x, y, forest, maxX)
			if score > topScore {
				topScore = score
			}
		}
	}

	return topScore
}

func RunD8a() {
	fmt.Println("Day 8A:", d8a(common.GetInput("day8/input.txt")))
}

func RunD8b() {
	fmt.Println("Day 8B:", d8b(common.GetInput("day8/input.txt")))
}
