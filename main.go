package main

import (
	"fmt"
	"os"
	"sort"
	"strings"

	"gitlab.com/ejstreet/advent-of-code-2022/day1"
	"gitlab.com/ejstreet/advent-of-code-2022/day2"
	"gitlab.com/ejstreet/advent-of-code-2022/day3"
	"gitlab.com/ejstreet/advent-of-code-2022/day4"
	"gitlab.com/ejstreet/advent-of-code-2022/day5"
	"gitlab.com/ejstreet/advent-of-code-2022/day6"
	"gitlab.com/ejstreet/advent-of-code-2022/day7"
	"gitlab.com/ejstreet/advent-of-code-2022/day8"
)

func main() {
	const version = "VERSION" // replaced by CI
	const help = `
Advent of Code 2022 Solutions
Author: Elliott Street
Written in: Go (Golang)
Version: VERSION

Usage:
  advent-of-code-2022 [OPTION]
or
  advent-of-code-2022 [DAY] [PART]
where DAY is a number between 1 and 25, and PART is 'A' or 'B'

OPTIONS:
a, all: Runs all solutions
v, version: Prints version of this binary
h, help: Prints this message`
	args := os.Args

	solutions := map[string]func(){
		"1a": day1.RunD1a,
		"1b": day1.RunD1b,
		"2a": day2.RunD2a,
		"2b": day2.RunD2b,
		"3a": day3.RunD3a,
		"3b": day3.RunD3b,
		"4a": day4.RunD4a,
		"4b": day4.RunD4b,
		"5a": day5.RunD5a,
		"5b": day5.RunD5b,
		"6a": day6.RunD6a,
		"6b": day6.RunD6b,
		"7a": day7.RunD7a,
		"7b": day7.RunD7b,
		"8a": day8.RunD8a,
		"8b": day8.RunD8b,
	}

	if len(args) == 2 {
		switch args[1] {
		case "a", "all":
			sorted := make([]string, 0)
			for k := range solutions {
				sorted = append(sorted, k)
			}
			sort.Strings(sorted)
			for k := range sorted {
				solutions[sorted[k]]()
			}
		case "v", "version":
			fmt.Println(version)
		case "h", "help":
			fmt.Println(help)
		default:
			fmt.Println(help)
		}
	} else if len(args) == 3 {
		solutions[args[1]+strings.ToLower(args[2])]()
	} else {
		fmt.Println(help)
	}
}
