package day6

import "testing"

const test_input = `bvwbjplbgvbhsrlpgdmjqwftvncz
`

func TestDay6a(t *testing.T) {
	result := d6a(test_input)
	expected := 5
	if result != expected {
		t.Log("Error: result should be ", expected, "- but got", result)
		t.Fail()
	}
}

func TestDay6b(t *testing.T) {
	result := d6b(test_input)
	expected := 23
	if result != expected {
		t.Log("Error: result should be ", expected, "- but got", result)
		t.Fail()
	}
}
