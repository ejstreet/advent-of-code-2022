package day6

import (
	"fmt"
	"strings"

	"gitlab.com/ejstreet/advent-of-code-2022/common"
)

func findUniqueSequence(input string, l int) int {
	for i := l - 1; i < len(input); i++ {

		slice := input[1+i-l : 1+i]

		duplicates := false

		for j := 0; j < l-1; j++ {
			if strings.Contains(slice[j+1:l], string(slice[j])) {
				duplicates = true
			}
		}
		if !duplicates {
			return i + 1
		}
	}
	return -1
}

// How many characters need to be processed before the first start-of-packet marker is detected?
func d6a(input string) int {
	return findUniqueSequence(input, 4)
}

// How many characters need to be processed before the first start-of-message marker is detected?
func d6b(input string) int {
	return findUniqueSequence(input, 14)
}

func RunD6a() {
	fmt.Println("Day 6A:", d6a(common.GetInput("day6/input.txt")))
}

func RunD6b() {
	fmt.Println("Day 6B:", d6b(common.GetInput("day6/input.txt")))
}
