package day4

import (
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/ejstreet/advent-of-code-2022/common"
)

// In how many assignment pairs does one range fully contain the other?
func d4a(input string) int {

	pairs := strings.Split(input, "\n")
	totalFullyContained := 0

	for i := 0; i < len(pairs); i++ {
		if pairs[i] != "" {
			sections := strings.Split(pairs[i], ",")

			a_low, _ := strconv.Atoi(strings.Split(sections[0], "-")[0])
			a_high, _ := strconv.Atoi(strings.Split(sections[0], "-")[1])
			b_low, _ := strconv.Atoi(strings.Split(sections[1], "-")[0])
			b_high, _ := strconv.Atoi(strings.Split(sections[1], "-")[1])

			if a_low >= b_low && a_high <= b_high || a_low <= b_low && a_high >= b_high {
				totalFullyContained++
			}
		}
	}

	return totalFullyContained
}

// In how many assignment pairs do the ranges overlap?
func d4b(input string) int {

	pairs := strings.Split(input, "\n")
	totalFullyContained := 0

	for i := 0; i < len(pairs); i++ {
		if pairs[i] != "" {
			sections := strings.Split(pairs[i], ",")

			a_low, _ := strconv.Atoi(strings.Split(sections[0], "-")[0])
			a_high, _ := strconv.Atoi(strings.Split(sections[0], "-")[1])
			b_low, _ := strconv.Atoi(strings.Split(sections[1], "-")[0])
			b_high, _ := strconv.Atoi(strings.Split(sections[1], "-")[1])

			if a_low >= b_low && a_low <= b_high {
				totalFullyContained++
			} else if a_high >= b_low && a_high <= b_high {
				totalFullyContained++
			} else if b_low >= a_low && b_low <= a_high {
				totalFullyContained++
			} else if b_high >= a_low && b_high <= a_high {
				totalFullyContained++
			}
		}
	}

	return totalFullyContained
}

func RunD4a() {
	fmt.Println("Day 4A:", d4a(common.GetInput("day4/input.txt")))
}

func RunD4b() {
	fmt.Println("Day 4B:", d4b(common.GetInput("day4/input.txt")))
}
