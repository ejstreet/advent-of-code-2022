package day4

import "testing"

const test_input = `2-4,6-8
2-3,4-5
5-7,7-9
2-8,3-7
6-6,4-6
2-6,4-8
`

func TestDay4a(t *testing.T) {
	result := d4a(test_input)
	expected := 2
	if result != expected {
		t.Log("Error: result should be ", expected, "- but got", result)
		t.Fail()
	}
}

func TestDay4b(t *testing.T) {
	result := d4b(test_input)
	expected := 4
	if result != expected {
		t.Log("Error: result should be ", expected, "- but got", result)
		t.Fail()
	}
}
