FROM scratch

COPY . .
COPY advent-of-code-2022 .

ENTRYPOINT ["./advent-of-code-2022"]
