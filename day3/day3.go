package day3

import (
	"fmt"
	"strings"

	"gitlab.com/ejstreet/advent-of-code-2022/common"
)

func findCommonItemInCompartments(items string) string {
	compSize := len(items) / 2
	compartment1 := items[compSize:]
	compartment2 := items[:compSize]

	for i := 0; i < compSize; i++ {
		item := string(compartment1[i])
		if strings.Contains(compartment2, item) {
			return item
		}
	}
	return ""
}

func findCommonItemBetween3Elves(items []string) string {

	for i := 0; i < len(items[0]); i++ {
		item := string(items[0][i])
		if strings.Contains(items[1], item) && strings.Contains(items[2], item) {
			return item
		}
	}
	return ""
}

func getPriority(item string) int {
	listOfPriorities := "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

	return strings.Index(listOfPriorities, item) + 1
}

// Find the item type that appears in both compartments of each rucksack.
// What is the sum of the priorities of those item types?
func d3a(input string) int {

	bags := strings.Split(input, "\n")
	totalPriority := 0

	for i := 0; i < len(bags); i++ {
		if bags[i] != "" {
			totalPriority += getPriority(findCommonItemInCompartments(bags[i]))
		}
	}

	return totalPriority
}

// Find the item type that corresponds to the badges of each three-Elf group.
// What is the sum of the priorities of those item types?
func d3b(input string) int {
	bags := strings.Split(input, "\n")
	totalPriority := 0

	for i := 0; i < len(bags); i += 3 {
		if bags[i] != "" {
			totalPriority += getPriority(findCommonItemBetween3Elves(bags[i : i+3]))
		}
	}

	return totalPriority
}

func RunD3a() {
	fmt.Println("Day 3A:", d3a(common.GetInput("day3/input.txt")))
}

func RunD3b() {
	fmt.Println("Day 3B:", d3b(common.GetInput("day3/input.txt")))
}
