package day7

import "testing"

const test_input = `$ cd /
$ ls
dir a
14848514 b.txt
8504156 c.dat
dir d
$ cd a
$ ls
dir e
29116 f
2557 g
62596 h.lst
$ cd e
$ ls
584 i
$ cd ..
$ cd ..
$ cd d
$ ls
4060174 j
8033020 d.log
5626152 d.ext
7214296 k
`

func TestDay7a(t *testing.T) {
	result := d7a(test_input)
	expected := 95437
	if result != expected {
		t.Log("Error: result should be ", expected, "- but got", result)
		t.Fail()
	}
}

func TestDay7b(t *testing.T) {
	result := d7b(test_input)
	expected := 24933642
	if result != expected {
		t.Log("Error: result should be ", expected, "- but got", result)
		t.Fail()
	}
}
