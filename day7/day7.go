package day7

import (
	"fmt"
	"sort"
	"strconv"
	"strings"

	"gitlab.com/ejstreet/advent-of-code-2022/common"
)

func cd(currentDir []string, arg string) []string {
	if arg == ".." {
		currentDir = currentDir[:len(currentDir)-1]
	} else if arg == "/" {
		currentDir = append(currentDir, "/")
	} else {
		currentDir = append(currentDir, arg)
	}

	return currentDir
}

func buildTree(lines []string) map[string]int {
	currentDir := make([]string, 0)
	fileTree := make(map[string]int)

	for l := range lines {
		line := strings.Split(lines[l], " ")

		if line[0] == "$" && line[1] == "cd" {
			currentDir = cd(currentDir, line[2])

		} else if line[0] != "dir" {
			size, _ := strconv.Atoi(line[0])
			fileTree[strings.Join(currentDir, "/")] += size
		}
	}
	return fileTree
}

func sortPaths(tree map[string]int) []string {
	paths := make([]string, 0, len(tree))
	for p := range tree {
		paths = append(paths, p)
	}

	sort.Strings(paths)
	return paths
}

func sumDirsToParents(tree map[string]int, paths []string) {
	for p := range paths {
		splitPath := strings.Split(paths[p], "/")
		for d := len(splitPath) - 2; d > 0; d-- {
			tree[strings.Join(splitPath[:d+1], "/")] += tree[paths[p]]
		}
	}
}

// Find all of the directories with a total size of at most 100000.
// What is the sum of the total sizes of those directories?
func d7a(input string) int {

	lines := strings.Split(input, "\n")

	fileTree := buildTree(lines)
	sortedPaths := sortPaths(fileTree)

	sumDirsToParents(fileTree, sortedPaths)

	total := 0

	for p := range sortedPaths {
		if fileTree[sortedPaths[p]] <= 100000 {
			total += fileTree[sortedPaths[p]]
		}
	}
	return total
}

// Find the smallest directory that, if deleted, would free
// up enough space on the filesystem to run the update.
// What is the total size of that directory?
func d7b(input string) int {

	lines := strings.Split(input, "\n")

	fileTree := buildTree(lines)
	sortedPaths := sortPaths(fileTree)

	const totalDiskSize int = 70000000
	const requiredSpace int = 30000000

	usedSpace := 0

	for dir := range fileTree {
		usedSpace += fileTree[dir]
	}

	reqDelete := requiredSpace - totalDiskSize + usedSpace
	sumDirsToParents(fileTree, sortedPaths)

	smallestEligibleSize := requiredSpace

	for dir := range fileTree {
		if fileTree[dir] >= reqDelete {
			if fileTree[dir] < smallestEligibleSize {
				smallestEligibleSize = fileTree[dir]
			}
		}
	}

	return smallestEligibleSize
}

func RunD7a() {
	fmt.Println("Day 7A:", d7a(common.GetInput("day7/input.txt")))
}

func RunD7b() {
	fmt.Println("Day 7B:", d7b(common.GetInput("day7/input.txt")))
}
